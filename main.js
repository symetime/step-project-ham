const servicesText = document.querySelector('.services-text');
const servicesImg = document.querySelector('.services-img');
let servItems = document.querySelectorAll('.services-item');
let currentSlide = 1;
let totalSlides = document.querySelectorAll('.face-item').length;
let mainFace = document.querySelector('.main-photo');

servItems.forEach(function(item) {
    item.addEventListener("click", function() {
        // Remove the "color" class from all elements
        servItems.forEach(function(innerItem) {
            innerItem.classList.remove("selected");
        });
        item.classList.add("selected");
    });
});

// List with text values for context block :^)
const content =  [
    'Web design encompasses many different skills and disciplines in the production and maintenance of websites',
    'Graphic design is a craft where professionals create visual content to communicate messages',
    'Customer Support is a range of services to assist customers in making cost effective and correct use of a product',
    'The worlds easiest-to-use design and ideation tool - powered by AI! No design experience required!',
    'Digital marketing is the component of marketing that uses the Internet and online-based digital technologies such as desktop computers',
    'Search engine optimization (SEO) is the process of improving the quality and quantity of website traffic to a website or a web page from search engines.'
]

let faceLinks = [
    './Step_Project_Ham/photo-about/gus.jpg',
    './Step_Project_Ham/photo-about/Mikola.jpg',
    './Step_Project_Ham/photo-about/hank.jpg',
    './Step_Project_Ham/photo-about/bob.jpg'
]

function chooseService(txtNum, link) {
    servicesText.textContent = content[txtNum];
    servicesImg.src = link;
}
document.querySelector(".load-btn").addEventListener("click", function() {

    let workElements = document.querySelectorAll(".work-element");

    for (let i = 0; i < 12 && i < workElements.length; i++) {
        workElements[i].classList.remove('work-element');
        workElements[i].classList.add('card-list-item');
    }

    if (workElements.length <= 12) {
        document.querySelector(".load-btn").style.display = "none";
    }
});

function filterItems(category) {
    let items = document.querySelectorAll('.card-list-item');

    items.forEach(function(item) {
        if (category === 'all' || item.getAttribute('data-category') === category) {
            item.style.display = 'block';
        } else {
            item.style.display = 'none';
        }
    });
}

let text = document.querySelector('.about-text');
let name = document.querySelector('.about-name');
let job = document.querySelector('.about-job');

function showSlide(slideNumber) {

    mainFace.src = faceLinks[slideNumber-1];


    switch (slideNumber){

        case 1:
            text.textContent = "Watch original and digitally remastered sermons by Derek Prince. Includes full message transcripts (free download).";
            name.textContent = "Gustavo Freg";
            job.textContent = "CEO 'Los Pollos'";
            break;
        case 2:
            text.textContent = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dolorum exercitationem in iusto labore possimus tempora? Assumenda, cupiditate debitis delectus doloremque eaque eveniet excepturi possimus quas quo recusandae tenetur vitae?";
            name.textContent = "Hasan Ali";
            job.textContent = "UX Designer'";
            break;
        case 3:
            text.textContent = "Computer security, cybersecurity, digital security or information technology security (IT security) is the protection of computer systems and networks from attacks by malicious actors that may result in unauthorized information disclosure, theft of, or damage to hardware, software, or data, as well as from the disruption or misdirection of the services they provide.";
            name.textContent = "Hank Thomas";
            job.textContent = "Chief Police Department";
            break;
        case 4:
            text.textContent = "International law (also known as public international law and the law of nations) is the set of rules, norms, and standards generally recognized as binding between states.";
            name.textContent = "Jack Co";
            job.textContent = "Lawyer";
            break;
    }

    document.querySelectorAll('.face-item').forEach(function(slide) {
        slide.classList.remove('selected-face');
    });
    document.getElementById('slide' + slideNumber).classList.add('selected-face');
}

function nextSlide() {
    currentSlide = (currentSlide % totalSlides) + 1;
    showSlide(currentSlide);
}

function prevSlide() {
    currentSlide = (currentSlide - 2 + totalSlides) % totalSlides + 1;
    showSlide(currentSlide);
}


